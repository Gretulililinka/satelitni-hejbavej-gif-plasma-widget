import QtQuick 2.11
import QtQuick.Window 2.2
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.11

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.components 3.0 as PlasmaComponents


Item
{
    
    Plasmoid.fullRepresentation: Item
    {
        id: main
        
        Layout.minimumWidth: 192 * PlasmaCore.Units.devicePixelRatio
        Layout.minimumHeight: 192 * PlasmaCore.Units.devicePixelRatio
        Layout.preferredWidth: 768 * PlasmaCore.Units.devicePixelRatio
        Layout.preferredHeight: 768 * PlasmaCore.Units.devicePixelRatio

        property int refreshInterval: plasmoid.configuration.refreshInterval
        property string zdroj: plasmoid.configuration.zdroj
        property bool zobrazovatMapku: plasmoid.configuration.zobrazovatMapku
        property real rychlostAnimace: plasmoid.configuration.rychlostAnimace
        property int skutecneFramu: plasmoid.configuration.skutecneFramu
        
        
        Timer
        {
            interval: 1000 * 60 * refreshInterval
            repeat: true
            running: true
            onTriggered:
                {
                    //znova načtem animaci
                    animation.source = "";
                    animation.source = zdroj;
                }
        }

        AnimatedImage 
        { 
            id: animation
            source: zdroj 
            speed: rychlostAnimace
            fillMode: Image.PreserveAspectFit
            anchors.fill: parent
            //cache: false
        
            onCurrentFrameChanged:
            {
                if(currentFrame >= skutecneFramu)
                {
                    //tendlecten fígl nám začne gif přehrávat znova vodzačátku
                    animation.playing = false
                    //console.log("jinej frame")
                    animation.playing = true
                }
            }

        }
        
        Image
        {
            source: "satelitMapa.svg"
            fillMode: Image.PreserveAspectFit
            anchors.fill: parent
            visible: zobrazovatMapku
        }
        
        Text
        {
            text: animation.status == Image.Loading ? 'Obrázek se načítá' : animation.status == Image.Error ? 'Chyba :D' : 'Obrázek neni'
            visible: animation.status != Image.Ready
            width: parent.width/4
            height: parent.height/4
            anchors.centerIn: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter

            color: '#00ffff'
            style: Text.Outline
            styleColor: '#0001fe'
                    
            font.pointSize: 256
            minimumPointSize: 8
            fontSizeMode: Text.Fit
        }
        
        Rectangle
        {
            id: prehravaniProgressBar
            x: animation.paintedWidth * 0.2 + (animation.width - animation.paintedWidth)/2
            y: animation.paintedHeight * 0.975 + (animation.height - animation.paintedHeight)/2
            width: animation.paintedWidth * 0.6
            height: animation.paintedHeight * 0.018
            color: "#101010"
            radius: width/4
            visible: animation.status == Image.Ready
            
            Rectangle
            {
                width: parent.width * ((animation.currentFrame + 1) / skutecneFramu)
                height: parent.height
                color: "#00ffff"
                radius: width/4
                
            }
            
            Rectangle
            {
                border.width: 2
                x: -border.width/2
                y: -border.width/2
                width: parent.width + border.width
                height: parent.height + border.width
                
                border.color: '#0001fe'
                radius: width/4
                color: "#00000000"
                
            }
    
        }

        
    }

}
