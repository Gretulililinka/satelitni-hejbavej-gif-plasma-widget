import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Controls 1 as StarodavnyQtControls
import QtQuick.Dialogs 1.0
import QtQuick.Layouts 1.12
import org.kde.kirigami 2.4 as Kirigami

Kirigami.FormLayout
{
    id: page
    Kirigami.FormData.label: i18n('Nastavení')
    
    // proměný musej mit ten prefix 'cfg_' aby si to jakože spárovalo s vodpovídajícíma hodnotama v souboru config.xml 
    property alias cfg_zdroj: konfiguraceAdresa.text
    property alias cfg_zobrazovatMapku: konfiguraceZobrazovatMapku.checked
    property alias cfg_rychlostAnimace: konfiguraceRychlostAnimace.value
    property alias cfg_refreshInterval: konfiguraceRefreshInterval.value
    property alias cfg_skutecneFramu: konfiguraceSkutecneFramu.value
    
    TextField
    {
        id: konfiguraceAdresa
        Kirigami.FormData.label: i18n('adresa zdrojovýho gifu:')
        placeholderText: i18n('https://neige.meteociel.fr/satellite/anim_ir.gif')
    }
    
    
    CheckBox
    {
        id: konfiguraceZobrazovatMapku
        Kirigami.FormData.label: i18n("kreslit přes to eště svg mapku:")
    }

        
    StarodavnyQtControls.SpinBox
    {
        id: konfiguraceRychlostAnimace
        decimals: 2
        stepSize: 0.1
        suffix: 'x'
        Kirigami.FormData.label: i18n('rychlost přehrávání animace:')
    }

    StarodavnyQtControls.SpinBox
    {
        id: konfiguraceRefreshInterval
        decimals: 0
        suffix: ' minut'
        Kirigami.FormData.label: i18n('refreshovávací interval obrázku:')
    }
    
    StarodavnyQtControls.SpinBox
    {
        id: konfiguraceSkutecneFramu
        decimals: 0
        minimumValue: 1
        Kirigami.FormData.label: i18n('kolik framů animace přehrávat:')
    }
    
}
 
